# **Requirements**
* Python3: pexpect, dbus
* QT5





# **Detecting phone unlock with dbus-monitor**

**Bash**
```bash
[nemo@Sailfish ~]$ dbus-monitor --session | grep /com/jolla/lipstick
signal time=1529970065.019872 sender=:1.36 -> destination=(null destination) serial=3831 path=/com/jolla/lipstick; interface=com.jolla.lipstick; member=coverstatus
```

**Python**
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pexpect
child = pexpect.spawn ('dbus-monitor --session')
child.expect ('/com/jolla/lipstick')
```





# **DBUS notications**

**Python**
```python
import dbus

bus = dbus.SessionBus()
object = bus.get_object('org.freedesktop.Notifications','/org/freedesktop/Notifications')
interface = dbus.Interface(object,'org.freedesktop.Notifications')

interface.Notify('app_name',
         0,
         'icon-m-notifications',
         'Test title',
         '<u>Test Body</u>\nLine 2 test',
         dbus.Array(['default', '']),
         dbus.Dictionary({'x-nemo-preview-body': 'Test Body Preview',
                          'x-nemo-preview-summary': 'Test Preview Summary'},
                          signature='sv'),
         0)
```

**QML**
```qml
Button {
    Notification {
        id: notification
        category: "x-nemo.example"
        appName: "Example App"
        appIcon: "/usr/share/example-app/icon-l-application"
        summary: "Notification summary"
        body: "Notification body"
        previewSummary: "Notification preview summary"
        previewBody: "Notification preview body"
        itemCount: 5
        timestamp: "2013-02-20 18:21:00"
        onClicked: console.log("Clicked")
        onClosed: console.log("Closed, reason: " + reason)
    }
    text: "Application notification" + (notification.replacesId ? " ID:" + notification.replacesId : "")
    onClicked: notification.publish()
}
```

**Relevant DBUS links**

http://prototype020.info/sailfishos-notification-categories.html